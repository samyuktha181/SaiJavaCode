FROM maven:3.8-openjdk-11 AS build
WORKDIR /usr/src/app
ARG COMMIT_ID=unknown
LABEL commit_id=$COMMIT_ID
COPY ./ ./
RUN mvn clean install -f pom.xml

RUN ls /usr/src/app/server/target
RUN ls /usr/src/app/webapp/target

FROM openjdk:11-jre-slim
WORKDIR /usr/src/app
COPY  --from=build /usr/src/app/server/target/server.jar .
RUN ls /usr/src/app
CMD ["java", "-jar", "server.jar"]